#!/bin/env python
import sys

class MinimumScalar(object):
    """
    The minimum scalar element will always come from the sum of multiplications
    between higher elements on vector1 and lower elements on vector2.
    _resolve_case does that in an onliner
    """
    def _resolve_case(self, n, v1, v2):
        minimum = sum([x*z for x, z in zip(sorted(v1),sorted(v2, reverse=True))])
        sys.stdout.write("Case #%s: %s\n" % (n, minimum))

    def __init__(self):
        self.cases = int(sys.stdin.readline())
        for i in range(1, self.cases+1):
            sys.stdin.readline()
            base_vector1 = [int(n) for n in sys.stdin.readline().split(" ")]
            base_vector2 = [int(n) for n in sys.stdin.readline().split(" ")]
            self._resolve_case(i, base_vector1, base_vector2)


if __name__ == "__main__":
    m = MinimumScalar()




