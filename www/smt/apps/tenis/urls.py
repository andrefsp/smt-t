from django.conf.urls import patterns, include, url
from smt.apps.tenis import views

urlpatterns = patterns('',
    url('^match/create/$', views.TenisMatchCreateView.as_view(), name="match-create"),
    url('^tournament/create/$', views.TenisTournamentCreateView.as_view(), name="tournament-create"),
    url('^player/create/$', views.TenisPlayerCreateView.as_view(), name="player-create"),
)
