from django.core.management.base import BaseCommand
from django.db.models.loading import get_model
from optparse import make_option
import csv


model_attr_map = {
        "ATP" :  ("tenis.ATP", "title"),
        "Location" : ("tenis.Location", "city"),
        "Series" : ("tenis.Series", "title"),
        "Court" : ("tenis.Court", "description"),
        "Surface" : ("tenis.Surface", "description"),
        "Round" : ("tenis.Round", "description"),
        # Tournament
        "Tournament" : ("tenis.Tournament", "name"),
        # Match model
        "Date" : ("tenis.Match", "date"),
        "Comment" : ("tenis.Match", "comment"),
        "Winner" : ("tenis.Match", "winner"),
        "Loser" : ("tenis.Match", "loser"),
        "Best of" : ("tenis.Match", "best_of"),
        "WRank" : ("tenis.Match", "winner_rank"),
        "LRank" : ("tenis.Match", "loser_rank"),
        "WPts" : ("tenis.Match", "winner_points"),
        "LPts" : ("tenis.Match", "loser_points"),
        "W1" : ("tenis.Match", "set1_winner"),
        "L1" : ("tenis.Match", "set1_loser"),
        "W2" : ("tenis.Match", "set2_winner"),
        "L2" : ("tenis.Match", "set2_loser"),
        "W3" : ("tenis.Match", "set3_winner"),
        "L3" : ("tenis.Match", "set3_loser"),
        "W4" : ("tenis.Match", "set4_winner"),
        "L4" : ("tenis.Match", "set4_loser"),
        "W5" : ("tenis.Match", "set5_winner"),
        "L5" : ("tenis.Match", "set5_loser"),
        "Wsets" : ("tenis.Match", "winner_sets"),
        "Lsets" : ("tenis.Match", "loser_sets"),
        "B365W" : ("tenis.Match", "b365_winner"),
        "B365L" : ("tenis.Match", "b365_loser"),
        "EXW" : ("tenis.Match", "ex_winner"),
        "EXL" : ("tenis.Match", "ex_loser"),
        "LBW" : ("tenis.Match", "lb_winner"),
        "LBL" : ("tenis.Match", "lb_loser"),
        "PSW" : ("tenis.Match", "ps_winner"),
        "PSL" : ("tenis.Match", "ps_loser"),
        "SJW" : ("tenis.Match", "sj_winner"),
        "SJL" : ("tenis.Match", "sj_loser"),
        "MaxW" : ("tenis.Match", "max_winner"),
        "MaxL" : ("tenis.Match", "max_loser"),
        "AvgW" : ("tenis.Match", "avg_winner"),
        "AvgL" : ("tenis.Match", "avg_loser"),
}

def cast_to(type):
    def caster(x):
        try:
            return type(x)
        except ValueError:
            return 0
    return caster


prepare_functions = {
        'Date': lambda x: '-'.join(x.split('/')[::-1]),
        "Best of" : cast_to(int),
        "WRank" : cast_to(int),
        "LRank" : cast_to(int),
        "WPts" : cast_to(int),
        "LPts" : cast_to(int),
        "W1" : cast_to(int),
        "L1" : cast_to(int),
        "W2" : cast_to(int),
        "L2" : cast_to(int),
        "W3" : cast_to(int),
        "L3" : cast_to(int),
        "W4" : cast_to(int),
        "L4" : cast_to(int),
        "W5" : cast_to(int),
        "L5" : cast_to(int),
        "Wsets" : cast_to(int),
        "Lsets" : cast_to(int),
        "B365W" : cast_to(float),
        "B365L" : cast_to(float),
        "EXW" : cast_to(float),
        "EXL" : cast_to(float),
        "LBW" : cast_to(float),
        "LBL" : cast_to(float),
        "PSW" : cast_to(float),
        "PSL" : cast_to(float),
        "SJW" : cast_to(float),
        "SJL" : cast_to(float),
        "MaxW" : cast_to(float),
        "MaxL" : cast_to(float),
        "AvgW" : cast_to(float),
        "AvgL" : cast_to(float),
}

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'
    option_list = BaseCommand.option_list + (
            make_option('--file',
                dest='file',
                default=False,
                help='Delete poll instead of closing it'),
            )

    def _manage_line(self, line):
        line_vals = {}
        for m in ('Series', 'ATP', 'Location', 'Round', 'Court', 'Surface',):
            model_name, attr_name= model_attr_map[m]
            model = get_model(*model_name.split("."))
            line_vals[m.lower()],__ = model.objects.get_or_create(
                    **{attr_name: line.pop(m)})

        # Players
        Player = get_model('tenis', 'Player')
        line_vals['winner'], __ = Player.objects.get_or_create(name=line.pop('Winner'))
        line_vals['loser'], __ = Player.objects.get_or_create(name=line.pop('Loser'))

        # Tournament
        Tournament = get_model('tenis', 'Tournament')
        line_vals['tournament'], __ = Tournament.objects.get_or_create(
                name=line.pop('Tournament'),
                location=line_vals['location'],
                atp=line_vals['atp'])

        # Match
        line_attrs = dict([(model_attr_map[key][1], val) for key, val in line.iteritems()])
        line_attrs.update(line_vals)

        line_attrs.pop('atp')               # only needed for tournament
        line_attrs.pop('location')          # only needed for tournament
        Match = get_model('tenis', 'Match')
        Match.objects.create(**line_attrs)


    def _reset_db(self):
        models_to_reset = set([model\
                for __, (model, __) in model_attr_map.iteritems()])
        for model in models_to_reset:
            get_model(*model.split(".")).objects.all().delete()

    def handle(self, *args, **options):
        if options.get('file', None):
            self._reset_db()
            for file_line in csv.DictReader(open(options['file'], 'r')):
                line = {}
                for key, val in file_line.iteritems():
                    fn = prepare_functions.get(key, None)
                    line[key] = fn(val) if fn else val
                self._manage_line(line)
