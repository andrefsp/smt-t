from django.db import models

class Series(models.Model):
    """
    Series model
    """
    title = models.CharField(max_length=256)

    def __str__(self):
        return "%s" % self.title

    def __unicode__(self):
        return u"%s" % self.__str__()


class ATP(models.Model):
    """
    ATP model
    """
    title = models.IntegerField()

    def __str__(self):
        return "%s" % self.title

    def __unicode__(self):
        return u"%s" % self.__str__()


class Location(models.Model):
    """
    Location model
    """
    city = models.CharField(max_length=256)

    def __str__(self):
        return "%s" % self.city

    def __unicode__(self):
        return u"%s" % self.__str__()


class Round(models.Model):
    """
    Round model
    """
    description = models.CharField(max_length=256)

    def __str__(self):
        return "%s" % self.description

    def __unicode__(self):
        return u"%s" % self.__str__()

class Court(models.Model):
    """
    Round model
    """
    description = models.CharField(max_length=256)

    def __str__(self):
        return "%s" % self.description

    def __unicode__(self):
        return u"%s" % self.__str__()

class Surface(models.Model):
    """
    Surface model
    """
    description = models.CharField(max_length=256)

    def __str__(self):
        return "%s" % self.description

    def __unicode__(self):
        return u"%s" % self.__str__()

class Tournament(models.Model):
    """
    Tournament model
    """
    name = models.CharField(max_length=256)
    location = models.ForeignKey('tenis.Location')
    atp = models.ForeignKey('tenis.ATP')

    def __str__(self):
        return "%s" % self.name

    def __unicode__(self):
        return u"%s" % self.name


class Player(models.Model):
    """
    Player model
    """
    name = models.CharField(max_length=256)

    def __str__(self):
        return "%s" % self.name

    def __unicode__(self):
        return u"%s" % self.__str__()

class Match(models.Model):
    """
    Match model
    """

    tournament = models.ForeignKey('tenis.Tournament')

    court = models.ForeignKey('tenis.Court')
    surface = models.ForeignKey('tenis.Surface')

    round = models.ForeignKey('tenis.Round')
    series = models.ForeignKey('tenis.Series')

    winner = models.ForeignKey('tenis.Player', related_name='winner_matches')
    loser = models.ForeignKey('tenis.Player', related_name='loser_matches')

    date = models.DateField()
    comment = models.CharField(max_length=256)

    winner_rank = models.IntegerField()
    loser_rank = models.IntegerField()

    winner_points = models.IntegerField()
    loser_points = models.IntegerField()

    best_of = models.IntegerField()
    winner_sets = models.IntegerField()
    loser_sets = models.IntegerField()

    set1_winner = models.IntegerField(null=True)
    set1_loser = models.IntegerField(null=True)
    set2_winner = models.IntegerField(null=True)
    set2_loser = models.IntegerField(null=True)
    set3_winner = models.IntegerField(null=True)
    set3_loser = models.IntegerField(null=True)
    set4_winner = models.IntegerField(null=True)
    set4_loser = models.IntegerField(null=True)
    set5_winner = models.IntegerField(null=True)
    set5_loser = models.IntegerField(null=True)


    # stats
    b365_winner = models.FloatField(null=True)
    b365_loser = models.FloatField(null=True)
    ex_winner = models.FloatField(null=True)
    ex_loser = models.FloatField(null=True)
    lb_winner = models.FloatField(null=True)
    lb_loser = models.FloatField(null=True)
    ps_winner = models.FloatField(null=True)
    ps_loser = models.FloatField(null=True)
    sj_winner = models.FloatField(null=True)
    sj_loser = models.FloatField(null=True)
    max_winner = models.FloatField(null=True)
    max_loser = models.FloatField(null=True)
    avg_winner = models.FloatField(null=True)
    avg_loser = models.FloatField(null=True)

    def ___str__(self):
        return "%s vs %s" % (self.winner, self.loser)

    def __unicode__(self):
        return u"%s" % self.__str__()

