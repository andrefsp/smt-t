from django.views import generic
from smt.apps.tenis import models
from smt.apps.tenis import forms


class TenisMatchCreateView(generic.edit.CreateView):
    "View to create new match records in the system"
    template_name = "tenis/match_create.html"
    model = models.Match
    form_class = forms.MatchForm


class TenisTournamentCreateView(generic.edit.CreateView):
    "View to create new tournaments in the system"
    template_name = "tenis/tournament_create.html"
    model = models.Tournament


class TenisPlayerCreateView(generic.edit.CreateView):
    "View for creating new players in the system"
    model = models.Player
    template_name = "tenis/player_create.html"

