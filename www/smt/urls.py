from django.conf.urls import patterns, include, url
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from smt import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'smt.views.home', name='home'),
    # url(r'^smt/', include('smt.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    #url('^$', lambda *args, **kwargs: HttpResponse("main page")),

    url('^$', views.HomeView.as_view()),
    url('^tenis/', include('smt.apps.tenis.urls', namespace="tenis", app_name="tenis")),
    url('^api/', include('smt.api.router')),
)
