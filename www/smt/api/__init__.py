from . import handlers
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'tournament', handlers.TournamentHandler)
router.register(r'player', handlers.PlayerHandler)
router.register(r'match', handlers.MatchHanlder)

