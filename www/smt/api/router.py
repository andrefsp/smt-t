from rest_framework import routers
from . import handlers

router = routers.DefaultRouter()
router.register(r'tournament', handlers.TournamentHandler)
router.register(r'player', handlers.PlayerHandler)
router.register(r'match', handlers.MatchHanlder)
router.register(r'location', handlers.LocationHandler)


urlpatterns = router.urls

