from rest_framework import viewsets
from rest_framework import renderers
from smt.api import serializers
from smt.apps.tenis import models as tenis_models


class JSONOnlyRendererMixin(object):
    renderer_classes = (renderers.JSONRenderer,)


class LocationHandler(JSONOnlyRendererMixin, viewsets.ModelViewSet):
    "Location handler"
    queryset = tenis_models.Location.objects.all()
    serializer_class = serializers.LocationSerializer


class TournamentHandler(JSONOnlyRendererMixin, viewsets.ModelViewSet):
    "Tournament hanlder"
    queryset = tenis_models.Tournament.objects.all()
    serializer_class = serializers.TournamentSerializer


class PlayerHandler(JSONOnlyRendererMixin, viewsets.ModelViewSet):
    "Player handler"
    queryset = tenis_models.Player.objects.all()
    serializer_class = serializers.PlayerSerializer


class MatchHanlder(JSONOnlyRendererMixin, viewsets.ModelViewSet):
    "Match handler"
    queryset = tenis_models.Match.objects.all()
    serializer_class = serializers.MatchSerializer


