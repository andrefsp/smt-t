from smt.apps.tenis import models as tenis_models
from rest_framework import serializers

class RoundSerializer(serializers.ModelSerializer):
    class Meta:
        model = tenis_models.Round


class SeriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = tenis_models.Series


class CourtSerializer(serializers.ModelSerializer):
    class Meta:
        model = tenis_models.Court


class SurfaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = tenis_models.Surface


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = tenis_models.Location


class TournamentSerializer(serializers.ModelSerializer):
    location = LocationSerializer()
    matches = serializers.SerializerMethodField('get_matches')

    def get_matches(self, obj):
        return MatchSerializer(obj.match_set.all(), many=True).data

    class Meta:
        model = tenis_models.Tournament


class PlayerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = tenis_models.Player


class MatchSerializer(serializers.HyperlinkedModelSerializer):
    winner = PlayerSerializer()
    loser = PlayerSerializer()
    surface = SurfaceSerializer()
    court = CourtSerializer()
    series = SeriesSerializer()
    round = RoundSerializer()
    tournament = serializers.HyperlinkedRelatedField(view_name='tournament-detail')
    class Meta:
        model = tenis_models.Match

